package com.example.gsc_30211.shopproject.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gsc_30211.shopproject.R;
import com.example.gsc_30211.shopproject.adapters_handlers.DatabaseHandler;
import com.example.gsc_30211.shopproject.model.UserModel;


public class SignUpActivity extends AppCompatActivity {
    private static final String TAG = "SignUpActivity";
    EditText _nameText,_mobText,_shopNameText,_addressText,_passwordText;
    TextView _loginLink;
    Button _signUpButton;
    DatabaseHandler db;
    public String name,mob_no,shop_name,address,password;
    UserModel userModel=new UserModel();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        _nameText=findViewById(R.id.input_name);
        _mobText=findViewById(R.id.input_mobile_no);
        _shopNameText=findViewById(R.id.input_shop_name);
        _addressText=findViewById(R.id.input_address);
        _passwordText=findViewById(R.id.input_password);
        _signUpButton=findViewById(R.id.btn_signup);
        _loginLink=findViewById(R.id.link_login);

        db=new DatabaseHandler(this);

        _signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login_intent=new Intent(SignUpActivity.this,LoginActivity.class);
                startActivity(login_intent);
                finish();
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signUpButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        // TODO: Implement your own signup logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        onSignupSuccess();
                        // onSignupFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    public void onSignupSuccess() {
        Log.d(TAG, "onSignupSuccess Values: "+name+" "+mob_no+" "+shop_name+" "+address+" "+password);
        _signUpButton.setEnabled(true);
        userModel.setUser_name(name);
        userModel.setMobile_no(mob_no);
        userModel.setShop_name(shop_name);
        userModel.setAddress(address);
        userModel.setPassword(password);
        userModel.setLoggedIn("false");
        if (userModel!=null){
            db.addUser(userModel);
            Toast.makeText(getApplicationContext(),"Registered Successfully",Toast.LENGTH_LONG).show();
            Intent intent=new Intent(SignUpActivity.this,LoginActivity.class);
            intent.putExtra("username",name);
            setResult(RESULT_OK, intent);
            finish();
        }

    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Sign up failed", Toast.LENGTH_LONG).show();

        _signUpButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

         name = _nameText.getText().toString();
         mob_no = _mobText.getText().toString();
         shop_name = _shopNameText.getText().toString();
         address = _addressText.getText().toString();
         password = _passwordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("at least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (mob_no.isEmpty() || !Patterns.PHONE.matcher(mob_no).matches()) {
            _mobText.setError("enter a valid email address");
            valid = false;
        } else {
            _mobText.setError(null);
        }

        if (address.isEmpty() || address.length()<3) {
            _addressText.setError("at least 3 characters");
            valid = false;
        } else {
            _addressText.setError(null);
        }

        if (shop_name.isEmpty() || shop_name.length()<3) {
            _shopNameText.setError("at least 3 characters");
            valid = false;
        } else {
            _shopNameText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}