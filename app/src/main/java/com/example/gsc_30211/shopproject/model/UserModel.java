package com.example.gsc_30211.shopproject.model;

/**
 * Created by GSC-30211 on 23-01-2018.
 */

public class UserModel {
    private int user_id;
    private String user_name;
    private String mobile_no;
    private String shop_name;
    private String address;
    private String password;
    private String isLoggedIn;


    public UserModel(int user_id, String user_name, String mobile_no, String shop_name, String address, String password, String isLoggedIn) {
        this.user_id=user_id;
        this.user_name = user_name;
        this.mobile_no = mobile_no;
        this.shop_name = shop_name;
        this.address = address;
        this.password = password;

        this.isLoggedIn=isLoggedIn;
    }
    public UserModel(String name, String mob_no, String shop_name, String address, String password,boolean b) {
        this.user_name = user_name;
        this.mobile_no = mobile_no;
        this.shop_name = this.shop_name;
        this.address = this.address;
        this.password = this.password;
    }

    public UserModel() {

    }


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(String loggedIn) {
        isLoggedIn = loggedIn;
    }


}
