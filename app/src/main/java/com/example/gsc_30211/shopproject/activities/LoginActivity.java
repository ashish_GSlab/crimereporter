package com.example.gsc_30211.shopproject.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gsc_30211.shopproject.R;
import com.example.gsc_30211.shopproject.adapters_handlers.DatabaseHandler;
import com.example.gsc_30211.shopproject.constants.Constants;
import com.example.gsc_30211.shopproject.model.UserModel;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    EditText _emailText, _passwordText;
    Button _loginButton;
    TextView _signupLink;
    String userName,password;
    SQLiteOpenHelper dbhelper;
    DatabaseHandler db;
    Cursor cursor;
    UserModel users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        _emailText=findViewById(R.id.input_email);
        _passwordText=findViewById(R.id.input_password);
        _loginButton=findViewById(R.id.btn_login);
        _signupLink=findViewById(R.id.link_signup);


        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent_signUp= new Intent(getApplicationContext(), SignUpActivity.class);
                startActivityForResult(intent_signUp, Constants.REQUEST_SIGN_UP);
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_SIGN_UP) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"Registered Successfully",Toast.LENGTH_LONG).show();
                userName=data.getStringExtra("username");
                Log.d(TAG, "onActivityResult: Username: "+userName);


            }
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);

        db=new DatabaseHandler(this);
        users = db.getUser(userName);
        // Reading all contacts
            String log = "Id: "+users.getUser_id()+" ,Name: " + users.getUser_name() + " ,password: " + users.getPassword();
            Log.d("Data: ", log+" Data2: ");

            if (userName.equals(users.getUser_name()) && password.equals(users.getPassword())){
                    Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                    startActivity(intent);

            }
            else {
                Toast.makeText(getApplicationContext(),"Invalid Username or Password",Toast.LENGTH_LONG).show();

            }

        //finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

         userName = _emailText.getText().toString();
         password = _passwordText.getText().toString();

        if (userName.isEmpty()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}