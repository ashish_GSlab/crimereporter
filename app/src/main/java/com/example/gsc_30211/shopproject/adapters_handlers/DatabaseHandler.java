package com.example.gsc_30211.shopproject.adapters_handlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.gsc_30211.shopproject.model.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GSC-30211 on 23-01-2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "ShopManager";

    // Users table name
    private static final String TABLE_USERS = "User";

    // Users Table Columns names
    private static final String KEY_ID = "Id";
    private static final String KEY_NAME = "Name";
    private static final String KEY_PH_NO = "Phone_number";
    private static final String KEY_SHOP_NAME = "Shop_name";
    private static final String KEY_ADDRESS = "Address";
    private static final String KEY_PASSWORD = "Password";
    private static final String KEY_ISLOGGEDIN = "IsLoggedIn";




    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_USERS +
                "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KEY_NAME + " TEXT," +
                KEY_PH_NO + " TEXT," +
                KEY_SHOP_NAME + " TEXT," +
                KEY_ADDRESS + " TEXT," +
                KEY_PASSWORD + " TEXT," +
                KEY_ISLOGGEDIN + " TEXT" +
                " )";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);

        // Create tables again
        onCreate(db);
    }

    // Adding new user
    public void addUser(UserModel user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, user.getUser_name()); // User Name
        values.put(KEY_PH_NO, user.getMobile_no()); // User Phone
        values.put(KEY_SHOP_NAME, user.getShop_name());
        values.put(KEY_ADDRESS, user.getAddress());
        values.put(KEY_PASSWORD, user.getPassword());
        values.put(KEY_ISLOGGEDIN,false);

        Log.d("DBHANDLER", "addUser: "+values);

        // Inserting Row
        db.insert(TABLE_USERS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single user
    public UserModel getUser(String name) {
        UserModel user = new UserModel();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO,KEY_SHOP_NAME,KEY_ADDRESS,KEY_PASSWORD,KEY_ISLOGGEDIN }, KEY_NAME+ "=?",
                new String[] { String.valueOf(name) }, null, null, null, null);
        Log.d("DBHANDLER", "getUser: CURSOR: "+cursor);
        if (cursor != null && cursor.moveToFirst()){

        user = new UserModel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4),
                cursor.getString(5),cursor.getString(6));}
        return user;
    }

    // Getting All Users
    public List<UserModel> getAllUsers() {
        List<UserModel> userList = new ArrayList<UserModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserModel user = new UserModel();
                user.setUser_id(Integer.parseInt(cursor.getString(0)));
                user.setUser_name(cursor.getString(1));
                user.setMobile_no(cursor.getString(2));
                user.setShop_name(cursor.getString(3));
                user.setAddress(cursor.getString(4));
                user.setPassword(cursor.getString(5));
                user.setPassword(cursor.getString(6));

                // Adding user to list
                userList.add(user);
            } while (cursor.moveToNext());
        }

        // return user list
        return userList;
    }

    // Updating single user
    public int updateUser(UserModel user) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("DB HANDLER", "updateUser: ID: "+String.valueOf(user.getUser_id()));


        ContentValues contentValues = new ContentValues();
//        contentValues.put(KEY_ID, user.getId());
        contentValues.put(KEY_NAME, user.getUser_name());
        contentValues.put(KEY_PH_NO, user.getMobile_no());
        contentValues.put(KEY_SHOP_NAME, user.getMobile_no());
        contentValues.put(KEY_ADDRESS, user.getMobile_no());
        contentValues.put(KEY_PASSWORD, user.getMobile_no());

        String whereClause = "KEY_ID=?";
        String whereArgs[] = {String.valueOf(user.getUser_id())};
        int i=db.update(TABLE_USERS, contentValues, whereClause, whereArgs);

//        Log.d("DB HANDLER", "updateUser: ID: "+user.getId());
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, user.getName());
//        values.put(KEY_PH_NO, user.getPhone());
//
////        int i = db.update(TABLE_USERS, values, KEY_ID + " = " + user.getId(), new String[] { String.valueOf(user.getId()) });
//        // updating row
//        return db.update(TABLE_USERS, values, KEY_ID + " = ?",
//                new String[] { String.valueOf(user.getId()) });
        return i;
    }

    // Deleting single user
    public void deleteUser(UserModel user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USERS, KEY_ID + " = ?",
                new String[] { String.valueOf(user.getUser_id()) });
        db.close();
    }


    // Getting users Count
    public int getUsersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_USERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


}

